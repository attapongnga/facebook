﻿using System;
using System.Linq;
using FacebookProjectTeam.Models;
using FacebookProjectTeam.Repository;

namespace FacebookProjectTeam.Controllers
{
    public interface IUserController
    {
        UserModels Login(string username, string password);
        UserModels SignupAddUser(string username, string password, string name);

        void SignUp(string username, string password, string name);

    }
   

    public class UserController : IUserController
    {
        private readonly FacebookDbContext _context;

        public UserController(FacebookDbContext context)
        {
            _context = context;
        }

        public UserModels Login(string username , string password)
        {
            try
            {

                var users = _context.TB_User.Where(user => user.username == username && user.password == password).FirstOrDefault();

                if (users == null)
                {
                    users = new UserModels();
                }

                return users;
            }
            catch (Exception e)
            {
                throw;
            }
        }

       
        public UserModels SignupAddUser(string username,string password,string name)
        {
            try
            {
                var users = _context.TB_User.Where(user => user.username == username).FirstOrDefault();
                if (users == null)
                {
                    users = new UserModels();
                }

                return users;
            }
            catch (Exception e)
            {
                throw;
            }
        }


        public void SignUp(string username,string password,string name)
        {
            UserModels data = new();
            data.username = username;
            data.password = password;
            data.name = name;

            _context.TB_User.Add(data);
            _context.SaveChanges();
        }

    }
}
